#ifndef _RANK_MERGESORT
#define _RANK_MERGESORT

#include "../LinkedList/linked_list.h"

struct ll_node *mergesort_fuse(struct ll_node *, struct ll_node *, int (*cmp)(void *, void *));
struct ll_node *mergesort_divide(int, struct ll_node *, int (*cmp)(void *, void *));
//struct ll_node *mergesort(int, struct ll_node *, int (*cmp)(void *, void *));

#endif
