#include "rank_mergesort.h"

struct ll_node *mergesort_fuse(struct ll_node *l, struct ll_node *r, int (*cmp)(void *, void *)) {
  struct ll_node *tmp, *out = NULL;

  if(l != NULL && r != NULL) {
    if(cmp(l->data, r->data) <= 0) {
      tmp = l;
      l = l->next;
    }
    else {
      tmp = r;
      r = r->next;
    }
    out = tmp;
  }

  while(l != NULL && r != NULL) {
    if(cmp(l->data, r->data) <= 0) {
      tmp->next = l;
      l = l->next;
    }
    else {
      tmp->next = r;
      r = r->next;
    }
    tmp = tmp->next;
  }
  if(l != NULL) {
    tmp->next = l;
  }
  if(r != NULL) {
    tmp->next = r;
  }

  return out;
}

struct ll_node *mergesort_divide(int n, struct ll_node *ll, int (*cmp)(void *, void *)) {
  int i;
  struct ll_node *l, *r, *m;

  if(n > 1) {
    for(i = 0, m = ll; ll != NULL && i < (n / 2) - 1; i++) { //seek midpoint
      m = m->next;
    }

    r = mergesort_divide(n - n / 2, m->next, cmp);
    m->next = NULL;
    l = mergesort_divide(n / 2, ll, cmp);
    
    
    return mergesort_fuse(l, r, cmp);
  }

  return ll;
}

/*struct ll_node *mergesort(int n, struct ll_node *ll, int (*cmp)(void *, void *)) {
  struct ll_node *h = mergesort_divide(n, ll, cmp);

  return h;
}*/