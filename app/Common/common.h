#ifndef _COMMON_H
#define _COMMON_H

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

#include "../Init/init.h"

#define is_nil(a) ((a)->path == NULL)
#define is_not_nil(a) ((a)->path != NULL)

#define get_vertice(n) (get_data(struct vertice, n))
#define get_path(n) (get_data(struct path, n))

struct oc_pb_config {
    int nnodes;
    int nconnec;
    int nmetrics;
};

struct tab_ops_consts {
    char *ops;
    char *consts;
};

struct vertice {
    short id;
};

struct path {
    double *metrics;

    short nvertices;
    struct ll_node *vertice;
};

struct path_disjoint_union {
    short npaths;

    struct ll_node *path;
};

extern void init_path_disjoint_union(struct path_disjoint_union *pdu);
struct path *new_path(short nmetrics);
void init_path(struct path *p, int nmetrics);
void init_vertice(struct vertice *v, int id);
struct vertice *new_vertice(int id);

void free_path(struct path *p);
struct path_disjoint_union *sumop(struct path_disjoint_union *r, struct path_disjoint_union *a);
struct path_disjoint_union *multop(struct path_disjoint_union *c, struct path_disjoint_union *a, struct path_disjoint_union *b, int x, double (*op[x])(double lm, double rm), double cst[x], bool (*cop[x])(double lm, double rm), bool is_init);



#endif
