#include "common.h"
#include "../Init/init.h"
#include "../LinkedList/linked_list.h"

void init_path_disjoint_union(struct path_disjoint_union *pdu) {
  pdu->path = NULL;
  pdu->npaths = 0;
}

struct path *new_path(short nmetrics) {
  struct path *p;

  if((p = (struct path *)malloc(sizeof(struct path))) == NULL) {
    error("failed malloc", EXIT_FAILURE);
  }

  init_path(p, nmetrics);

  return p;
}

void init_path(struct path *p, int nmetrics) {
  if((p->metrics = (double *)malloc(sizeof(double) * nmetrics)) == NULL) {
    error("failed malloc", EXIT_FAILURE);
  }

  p->vertice = NULL;
  p->nvertices = 0;
}

struct vertice *new_vertice(int id) {
  struct vertice *v;

  if((v = (struct vertice *)malloc(sizeof(struct vertice))) == NULL) {
    error("failed malloc", EXIT_FAILURE);
  }

  init_vertice(v, id);

  return v;
}

void init_vertice(struct vertice *v, int id) {
  v->id = id;
}

void free_path(struct path *p) {
    assert(p != NULL && p->metrics != NULL);

    free(p->metrics);
    free(p);
}

struct path_disjoint_union *sumop(struct path_disjoint_union *r, struct path_disjoint_union *a) {
    int i;

    struct ll_node *na;
    struct path *pa;

    na = a->path;
    for(i = 0; i < a->npaths; ++i) {
        pa = get_path(na);
        ll_add_node(&r->path, pa);

        na = na->next;
    }
    r->npaths += i;

    return r;
}

static inline bool is_cyclic(struct path *l, struct path *r) {
    int i;
    struct ll_node *n;
    struct vertice *vr = get_vertice(r->vertice);

    for(i = 0, n = l->vertice; i < l->nvertices &&
    get_vertice(n)->id != vr->id; ++i, n = n->next);

    return i != l->nvertices;
}

static inline bool is_violating_constraints(int x, double o[x], struct path *l, struct path *r, double (*mop[x])(double m0, double m1), double c[x], bool (*cop[x])(double m0, double m1), bool is_init) {
    bool sat = true;

    for(int i = 0; i < x && sat; ++i) {
        if(is_init)
        {
            o[i] = r->metrics[i];
        }
        else{
            o[i] = mop[i](l->metrics[i], r->metrics[i]);//calcul des metrics
        }
        sat &= cop[i](o[i], c[i]);//vérification des contraintes
    }

    return !sat;
}

struct path_disjoint_union *multop(struct path_disjoint_union *c, struct path_disjoint_union *a, struct path_disjoint_union *b, int x, double (*op[x])(double lm, double rm), double cst[x], bool (*cop[x])(double lm, double rm), bool is_init) {
    int i, j, k;
    double tmp[x];

    if(is_not_nil(a) && is_not_nil(b)) {
        struct vertice *vc;
        struct ll_node *na, *nb, *nc;
        struct path *pc, *pa, *pb;

        na = a->path;
        nb = b->path;
        pb = (struct path *)(nb->data);
        for(j = 0; j < a->npaths; ++j) {
            pa = (struct path *)(na->data);

            //cycle detection - skip path
            if(is_cyclic(pa, pb) ||
               is_violating_constraints(x, tmp, pa, pb, op, cst, cop, is_init)) {
                na = na->next;
                continue;
            }

            pc = new_path(x);
            for(i = 0; i < x; ++i) {
                pc->metrics[i] = tmp[i];
            }

            vc = new_vertice(((struct vertice *)pb->vertice->data)->id);
            pc->vertice = pa->vertice;
            pc->nvertices = pa->nvertices + 1;

            ll_add_node(&pc->vertice, vc);
            ll_add_node(&c->path, pc);

            c->npaths += 1;
            na = na->next;
        }
    }
    return c;
}
