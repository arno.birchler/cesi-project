#include "error.h"

void error_f(char *s, int errno, int ln) {
  fprintf(stderr, "Error (%d) : %s, at line %d.", errno, s, ln);
  exit(errno);
}