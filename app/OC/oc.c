#include "oc.h"
#include "../PrettyPrinter/pretty_printer.h"

void swapm(int n, struct path_disjoint_union (**a)[n], struct path_disjoint_union (**b)[n]) {
  struct path_disjoint_union (*s)[n];

  s = *a;
  *a = *b;
  *b = s;
}

#define RESET_PATH_UNION(a) (a.npaths = 0)

void oc(int n, //nb nodes
        struct path_disjoint_union r0[n][n],
        struct path_disjoint_union a0[n][n],
        int x,  //nombre de metrics
        double (*op[x])(double metric1, double metric2),
        double cst[x], //tableau des contraintes
        bool (*cop[x])(double metric1, double metric2)) {
  int i, j, k, e;
  bool updated;

  struct path_disjoint_union c0[n][n], b[n][n], d[n][n];
  struct path_disjoint_union (*r)[n] = r0;
  struct path_disjoint_union (*a)[n] = a0;
  struct path_disjoint_union (*c)[n] = c0;

  for(i = 0; i < n; ++i) {
    for(j = 0; j < n; ++j) {
        //init given matrix (set path to null and npath to 0)
      init_path_disjoint_union(&b[i][j]);
      init_path_disjoint_union(&c[i][j]);
      init_path_disjoint_union(&d[i][j]);

      if(a[i][j].path != NULL) {
        ll_add_node(&b[i][j].path, a[i][j].path->data);
        b[i][j].npaths = 1;
      }

      init_path_disjoint_union(&r[i][j]);
      RESET_PATH_UNION(r[i][j]);
    }
  }

  struct path *p;
  struct vertice *v;
  for(i = 0; i < n; ++i) {
    p = new_path(x);
    v = new_vertice(i);

    ll_add_node(&p->vertice, v);
    ll_add_node(&d[i][i].path, p);
    d[i][i].npaths = 1;
    p->nvertices = 1;
  }
  /*pp_psi(n, a, x);
  pp_psi(n, b, x);
  pp_psi(n, c, x);
  pp_psi(n, d, x);*/
  struct path_disjoint_union tmp;
  for(i = 0; i < n; ++i) {
    for(j = 0; j < n; ++j) {
      for(k = 0; k < n; ++k) {
        init_path_disjoint_union(&tmp);
        sumop(&c[i][j], multop(&tmp, &d[i][k], &a[k][j], x, op, cst, cop, true));
      }
    }
  }
  pp_psi(n, c, x);
    //a c'est la matrice résultante
    //c c'est une matrice temp
    //b c'est la matrice utiliser pour bouclé sur le calcul (matrice init avec chemin de 1) !!! PAS TOUCHER
    //d est uniquement utilisé pour init
  swapm(n, &c, &a);

  for(e = 0, updated = true; e < n && updated; ++e) {
    for(i = 0; i < n; ++i) {
      for(j = 0; j < n; ++j) {
        init_path_disjoint_union(&c[i][j]);
        for(k = 0; k < n; ++k) {
          init_path_disjoint_union(&tmp);
          sumop(&c[i][j], multop(&tmp, &a[i][k], &b[k][j], x, op, cst, cop, false));
        }
      }
    }
pp_psi(n, c, x);
    for(i = 0, updated = false; i < n; ++i) {  /* copy back solutions in result matrix */
      for(j = 0; j < n; ++j) {
        sumop(&r[i][j], &c[i][j]);

        updated |= is_not_nil(&c[i][j]);
      }
    }

    swapm(n, &c, &a);

  }
  #ifndef NDEBUG
    fprintf(stderr, "PRINTING FINAL MATRIX\n");
  #endif
  //ppl_psi(n, r, nmetrics);
}


