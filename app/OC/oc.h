#ifndef _OC_H
#define _OC_H

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "../LinkedList/linked_list.h"
#include "../Common/common.h"

void oc(int n, struct path_disjoint_union r0[n][n], struct path_disjoint_union a0[n][n], int x, double (*op[x])(double metric1, double metric2), double cst[x], bool (*cop[x])(double metric1, double metric2));
void swapm(int n, struct path_disjoint_union (**a)[n], struct path_disjoint_union (**b)[n]);

#endif