#ifndef _PARSER_H
#define _PARSER_H

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "../Common/common.h"

int read_pb_config(struct oc_pb_config *cnf, FILE *s);
void init_adj_matrix(int n, struct path_disjoint_union a[n][n]);
struct tab_ops_consts read_from_file(struct oc_pb_config cnf, FILE *s, struct path_disjoint_union a[cnf.nnodes][cnf.nnodes], double cst[cnf.nmetrics]);

#endif