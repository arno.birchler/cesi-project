#include "parser.h"
#include "../Init/init.h"
#include "../LinkedList/linked_list.h"

int read_pb_config(struct oc_pb_config *cnf, FILE *s) {
  int n, ntok;

  if(s == NULL) { goto error0; }

  ntok = fscanf(s, "%d %d %d", &cnf->nnodes, &cnf->nconnec, &cnf->nmetrics);

  if(ntok != 3 || cnf->nnodes < 0 || cnf->nconnec < 0 ||
    cnf->nmetrics < 0 || cnf->nconnec > cnf->nnodes * cnf->nnodes) {
    goto error1;
  }

  return n;

  error0:
    error("input failure : null file handler", EXIT_FAILURE);
  error1:
    error("input failure : incorrect input matrix format", EXIT_FAILURE);

  return -1;
}

void init_adj_matrix(int n, struct path_disjoint_union a[n][n]) {
  int i, j;

  for(i = 0; i < n; ++i) {
    for(j = 0; j < n; ++j) {
      init_path_disjoint_union(&a[i][j]);
    }
  }
}

#define NODE_ID_RENORMALIZATION(a) (a = a - 1)

struct tab_ops_consts read_from_file(struct oc_pb_config cnf, FILE *s, struct path_disjoint_union a[cnf.nnodes][cnf.nnodes], double cst[cnf.nmetrics]) {
  int i, j, k, ntok;
  double m;

  struct path *p;
  struct vertice *v;
  struct tab_ops_consts ops_consts;

  if(s == NULL) { goto error0; }

  //reading constraints //2nd line
  for(k = 0; k < cnf.nmetrics; ++k) {
      fscanf(s, "%lf", &cst[k]);
  }

  ops_consts.ops = malloc(cnf.nmetrics * sizeof(char));
  ops_consts.consts = malloc(cnf.nmetrics * sizeof(char));

  if (ops_consts.ops == NULL) {
      error("OPS allocation failed\n", EXIT_FAILURE);
    }


  if (ops_consts.consts == NULL) {
      error("consts allocation failed\n", EXIT_FAILURE);
    }

  //reading operators
  for(k = 0; k < cnf.nmetrics; ++k) {
        fscanf(s, "%s", &ops_consts.ops[k]);
  }

  //reading constraints
  for(k = 0; k < cnf.nmetrics; ++k) {
      fscanf(s, "%s", &ops_consts.consts[k]);
  }

  //reading adjacency graph
while(fscanf(s, "%d %d", &i, &j) != EOF) {
    NODE_ID_RENORMALIZATION(i);
    NODE_ID_RENORMALIZATION(j);

    init_path_disjoint_union(&a[i][j]);

    p = new_path(cnf.nmetrics);
    for(k = 0; k < cnf.nmetrics; ++k) {
      fscanf(s, "%lf", &m);

      p->metrics[k] = m;
    }
    v = new_vertice(j);
    ll_add_node(&p->vertice, v);
    ll_add_node(&a[i][j].path, p);

    p->nvertices = 1;
    a[i][j].npaths = 1;
  }

  #ifndef NDEBUG
    fprintf(stderr, "INPUT FILE HAS BEEN READ\n");
  #endif

  return ops_consts;

  error0:
    error("input failure : null file handler", EXIT_FAILURE);
    //return;
  error1:
    error("input failure : incorrect input matrix format", EXIT_FAILURE);
}
