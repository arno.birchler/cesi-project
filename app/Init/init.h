#ifndef _INIT_H
#define _INIT_H

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "../error.h"

void init_metrics(char *ops, int nmetrics, double (*mop[nmetrics])(double metric1, double metric2));
void init_constraint_operators(char *consts, int x, bool (*cop[x])(double metric1, double metric2));

double sum_op(double metric1, double metric2);
bool lte(double metric1, double metric2);

#endif
