#include "init.h"
//Used by init.c
double sum_op(double metric1, double metric2) {
  return metric1 + metric2;
}

double less_op(double metric1, double metric2) {
  return metric1 - metric2;
}

double max_op(double metric1, double metric2) {
    if(metric1 > metric2)
    {
        return metric1;
    }
    else
    {
        return metric2;
    }
}

double min_op(double metric1, double metric2) {
      if(metric1 < metric2)
    {
        return metric1;
    }
    else
    {
        return metric2;
    }
}

double multi_op(double metric1, double metric2) {
  return metric1 * metric2;
}

double div_op(double metric1, double metric2) {
  return metric1 / metric2;
}

//metrics must be computed at each step.
bool gt(double metric1, double metric2) {
  return metric1 > metric2;
}

bool gte(double metric1, double metric2) {
  return metric1 >= metric2;
}

bool eq(double metric1, double metric2) {
  return metric1 = metric2;
}

bool lt(double metric1, double metric2) {
  return metric1 < metric2;
}

bool lte(double metric1, double metric2) {
  return metric1 <= metric2;
}



void init_metrics(char* ops, int nmetrics, double (*mop[nmetrics])(double metric1, double metric2)) {
  int i;

  for(i = 0; i < nmetrics; ++i) {
    //mop[i] = sum_op;
    mop[i] = ops[i];
    if (ops[i] == '+')
    {
      mop[i] = sum_op;
    }
    else if (ops[i] == '-')
    {
      mop[i] = less_op;
    }
    else if (ops[i] == 'M')
    {
      mop[i] = max_op;
    }
    else if (ops[i] == 'm')
    {
      mop[i] = min_op;
    }
    else if (ops[i] == '*')
    {
      mop[i] = multi_op;
    }
    else if (ops[i] == '/')
    {
      mop[i] = div_op;
    }
    else
    {

    }


  }
}

void init_constraint_operators(char *consts, int x, bool (*cop[x])(double metric1, double metric2)) {
  int i;

  for(i = 0; i < x; ++i) {
    /*cop[i] = lte;*/
    cop[i] = consts[i];

    if (consts[i] == 'L')
    {
      cop[i] = lte;
    }
    else if (consts[i] == 'l')
    {
      cop[i] = lt;
    }
    else if (consts[i] == 'e')
    {
      cop[i] = eq;
    }
    else if (consts[i] == 'g')
    {
      cop[i] = gt;
    }
    else if (consts[i] == 'G')
    {
      cop[i] = gte;
    }
    else{

    }

    /*cop[i] = consts[i];

    switch (consts[i][0] && consts[i][1] && consts[i][2])
    {
      case consts[i][0] == '+':
        mop[i] = sum_op;
        break;
      case '-':
        mop[i] = less_op;
        break;
      case "max":
        mop[i] = max_op;
        break;
      case "min":
        mop[i] = min_op;
        break;
      case '*':
        mop[i] = multi_op;
        break;
      case '/':
        mop[i] = div_op;
        break;
      default:
        break;
    }*/
  }
}
