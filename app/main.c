/**
Author: Jean-François DOLLINGER
*/

#include "main.h"

#include "PrettyPrinter/pretty_printer_latex.h"
#include "Rank/rank_mergesort.h"
#include "Parser/parser.h"

#define RESET_PATH_DISJOINT_UNION(a) (a.npaths = 0, a.path = NULL)

double maxop(double l, double r) {
  return max(l, r);
}

int main(int argc, char *argv[])
{
  if (argc < 2) {
    error("Need graph file as argument", EXIT_FAILURE);
  }

  printf("graph use : '%s'\n", argv[1]);

  FILE *f = fopen(argv[1], "r");

  struct oc_pb_config cnf;
  //oc(N, r, a, op);

  read_pb_config(&cnf, f);

  double (*mop[cnf.nmetrics])(double metric1, double metric2);
  bool (*cop[cnf.nmetrics])(double metric1, double metric2);

  double cst[cnf.nmetrics]; //tableau des contraintes
  struct path_disjoint_union r[cnf.nnodes][cnf.nnodes];
  struct path_disjoint_union a[cnf.nnodes][cnf.nnodes];

  init_adj_matrix(cnf.nnodes, a);

  struct tab_ops_consts ops_consts = read_from_file(cnf, f, a, cst);

  init_metrics(ops_consts.ops, cnf.nmetrics, mop);
  init_constraint_operators(ops_consts.consts, cnf.nmetrics, cop);

  //print_adjacency_matrix(cnf.nnodes, a, cnf.nmetrics);

  oc(cnf.nnodes, r, a, cnf.nmetrics, mop, cst, cop);

  //pp_psi(cnf.nnodes, r, cnf.nmetrics);
  //pp_path_disjoint_union(&r[0][0], cnf.nmetrics);

  //mergesort(0, NULL, NULL);

  return 0;
}
