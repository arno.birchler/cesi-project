#ifndef _PP_H
#define _PP_H

#include <stdio.h>
#include <stdlib.h>

#include "../OC/oc.h"
#include "../LinkedList/linked_list.h"


void pp_vertice(struct ll_node *n);
static void pp_vertice_node(struct ll_node *n);
void pp_vertice_list(struct ll_node *v);
static void pp_metric(double m);
void pp_metrics(int x, double m[x]);
void pp_path(struct path *p, int x);
void pp_path_disjoint_union_list(struct ll_node *pdu, int x);
void pp_path_disjoint_union(struct path_disjoint_union *pdu, int x);
void pp_psi(int n, struct path_disjoint_union a[n][n], int x);


#endif
