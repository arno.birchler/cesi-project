#include "pretty_printer_latex.h"


static void ppl_vertice(struct ll_node *n) {
  printf("%d", ((struct vertice *)n->data)->id + 1);
}

static void ppl_vertice_node(struct ll_node *n) {
  if(n != NULL) {
    ppl_vertice_node(n->next);
    ppl_vertice(n);
    printf(", ");
  }
}

void ppl_vertice_list(struct ll_node *v) {
  if(v != NULL) {
    ppl_vertice_node(v->next);
    ppl_vertice(v);
  }
}

static void ppl_metric(double m) {
  printf("%lf", m);
}

void ppl_metrics(int x, double m[x]) {
  for(int i = 0; i < x - 1; ++i) {
    ppl_metric(m[i]);
    printf(", ");
  }
  if(x > 0) {
    ppl_metric(m[x - 1]);
  }
}

void ppl_path(struct path *p, int x) {
  int i;
  struct ll_node *n;
  struct vertice *v;

  printf("\\overset{(");
  ppl_metrics(x, p->metrics);
  printf(")}");

  printf("{\\epsilon_{\\omega_{\\{");
  ppl_vertice_list(p->vertice);
  printf("\\}}}}");
}

void ppl_path_disjoint_union_list(struct ll_node *pdu, int x) {
  if(pdu != NULL) {
    ppl_path_disjoint_union_list(pdu->next, x);
    ppl_path((struct path *)pdu->data, x);
  }
  
}

void ppl_path_disjoint_union(struct path_disjoint_union *pdu, int x) {
  if(pdu->path != NULL) {
    ppl_path_disjoint_union_list(pdu->path->next, x);
    ppl_path((struct path *)pdu->path->data, x);
  }
  else {
    printf("0");
  }
  printf(" & ");
}

void ppl_psi(int n, struct path_disjoint_union a[n][n], int x) {
  int i, j;

  for(i = 0; i < n; ++i) {
    for(j = 0; j < n; ++j) {
      ppl_path_disjoint_union(&a[i][j], x);
    }
    printf(" \\\\\n");
  }
}