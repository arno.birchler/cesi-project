#ifndef _PP_LATEX_H
#define _PP_LATEX_H

#include <stdio.h>
#include <stdlib.h>

#include "../OC/oc.h"
#include "../LinkedList/linked_list.h"


static void ppl_vertice(struct ll_node *n);
static void ppl_vertice_node(struct ll_node *n);
void ppl_vertice_list(struct ll_node *v);
static void ppl_metric(double m);
void ppl_metrics(int x, double m[x]);
void ppl_path(struct path *p, int x);
void ppl_path_disjoint_union_list(struct ll_node *pdu, int x);
void ppl_path_disjoint_union(struct path_disjoint_union *pdu, int x);
void ppl_psi(int n, struct path_disjoint_union a[n][n], int x);


#endif
