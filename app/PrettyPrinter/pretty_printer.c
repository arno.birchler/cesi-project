#include "pretty_printer.h"


void pp_vertice(struct ll_node *n) {
  printf("%d", ((struct vertice *)n->data)->id + 1);
}

static void pp_vertice_node(struct ll_node *n) {
  if(n != NULL) {
    pp_vertice_node(n->next);
    pp_vertice(n);
    printf(", ");
  }
}

void pp_vertice_list(struct ll_node *v) {
  printf("<");
  if(v != NULL) {
    pp_vertice_node(v->next);
    pp_vertice(v);
  }
  printf(">");
}

static void pp_metric(double m) {
  printf("%lf", m);
}

void pp_metrics(int x, double m[x]) {
  for(int i = 0; i < x - 1; ++i) {
    pp_metric(m[i]);
    printf(", ");
  }
  if(x > 0) {
    pp_metric(m[x - 1]);
  }
}

void pp_path(struct path *p, int x) {
  int i;
  struct ll_node *n;
  struct vertice *v;

  printf("{ path: ");
  pp_vertice_list(p->vertice);

  printf(", weight: <");
  pp_metrics(x, p->metrics);
  printf("> }");
}

void pp_path_disjoint_union_list(struct ll_node *pdu, int x) {
  if(pdu != NULL) {
    pp_path_disjoint_union_list(pdu->next, x);
    pp_path((struct path *)pdu->data, x);
    printf(", ");
  }
}

void pp_path_disjoint_union(struct path_disjoint_union *pdu, int x) {
  if(pdu->path != NULL) {
    printf("{ ");
    pp_path_disjoint_union_list(pdu->path->next, x);
    pp_path((struct path *)pdu->path->data, x);
    printf(" }\n");
  }
}

void pp_psi(int n, struct path_disjoint_union a[n][n], int x) {
  int i, j;

  for(i = 0; i < n; ++i) {
    for(j = 0; j < n; ++j) {
      pp_path_disjoint_union(&a[i][j], x);
    }
  }
}