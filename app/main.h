#ifndef _MAIN_H
#define _MAIN_H

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include <limits.h>
#include "PrettyPrinter/pretty_printer.h"

#define max(a, b) (((a) >= (b)) ? (a) : (b))

void error_f(char *, int, int);

//int nmetrics;

#endif