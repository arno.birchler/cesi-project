#include "linked_list.h"

void ll_init_node(struct ll_node *n, void *data) {
  assert(n != NULL);

  n->data = data;
  n->next = NULL;
}

void ll_add_node(struct ll_node **n, void *data) {
  struct ll_node *tmp;
  
  if((tmp = (struct ll_node *)malloc(sizeof(struct ll_node))) == NULL){
    error("failed malloc", EXIT_FAILURE);
  }
  
  ll_init_node(tmp, data);

  if(*n != NULL) {
    tmp->next = *n;
  }
  *n = tmp;
}
