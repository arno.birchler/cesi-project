#ifndef _LL_H
#define _LL_H

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "../error.h"

struct ll_node {
  void *data;
  struct ll_node *next;
};

void ll_init_node(struct ll_node *, void *);
void ll_add_node(struct ll_node **, void *);

#define get_data(t, n) ((t *)n->data)

#endif