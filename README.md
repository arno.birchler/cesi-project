Cesi-project
============

# File format

All value are seprated by a space.
Two first lines are for headers, for the first there are three values,
the first value is the number of point, the second value is the number of paths (between each point) and the last value is the number of metrics (for each path).
The second line are for constraints, dependings the number of constraints you need.
After the header you declare each paths. The first two values are the begin point and the end pont of the path, next you put metrics for each paths.

For exemple :

```
6 9 3
1 250 1
1   2   0.41    2   0.31
1   3   0.01    22  0.19
2   3   0.19    37  0.39
2   4   0.07    21  0.03
3   4   0.32    33  0.09
3   5   0.44    6   0.45
4   5   0.12    35  0.07
4   6   0.08    17  0.19
5   6   0.44    43  0.39
```
