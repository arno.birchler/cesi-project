// from : http://cunit.sourceforge.net/example.html

#include <stdio.h>

#include <CUnit/Basic.h>

#include "init_test.h"
#include "commont_test.h"

/* Pointer to the file used by the tests. */
//static FILE* temp_file = NULL;

int init_suite1(void)
{
    /*
   if (NULL == (temp_file = fopen("temp.txt", "w+"))) {
      return -1;
   }
   else {
      return 0;
   }*/
   return 0;
}

/* The suite cleanup function.
 * Closes the temporary file used by the tests.
 * Returns zero on success, non-zero otherwise.
 */
int clean_suite1(void)
{
    /*
   if (0 != fclose(temp_file)) {
      return -1;
   }
   else {
      temp_file = NULL;
      return 0;
   }*/
   return 0;
}

int main()
{
    CU_pSuite pSuite = NULL;
    
    /* initialize the CUnit test registry */
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    /* add a suite to the registry */
    pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    
    /* add the tests to the suite */
    /* NOTE - ORDER IS IMPORTANT - MUST TEST fread() AFTER fprintf() */
    if (
        (NULL == CU_add_test(pSuite, "init::sum_up()", test_SUM_UP))
        || (NULL == CU_add_test(pSuite, "init::lte()", test_LTE))
        || (NULL == CU_add_test(pSuite, "common::init_path_disjoint_union()", test_INIT_PATH_DISJOINT_UNION))
        || (NULL == CU_add_test(pSuite, "common::init_path()", test_INIT_PATH))
       )
    {
        CU_cleanup_registry();
        return CU_get_error();
    }
    
    /* Run all tests using the CUnit Basic interface */
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    int nb_error = CU_get_number_of_failures();
    CU_cleanup_registry();
    return nb_error;
}
