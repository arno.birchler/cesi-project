#ifndef T_INIT_H
#define T_INIT_H

#include <CUnit/Basic.h>

#include "../app/Init/init.h"

void test_SUM_UP(void)
{
    CU_ASSERT_EQUAL(sum_op(5, 3), 8);
}

void test_LTE(void)
{
    CU_ASSERT_TRUE(lte(5.3, 5.5));
    CU_ASSERT_TRUE(lte(5, 5));
    CU_ASSERT_FALSE(lte(5.7, 5.5));
}

#endif // T_INIT_H