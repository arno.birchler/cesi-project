#ifndef T_COMMON_H
#define T_COMMON_H

#include <CUnit/Basic.h>

#include "../app/Common/common.h"

void test_INIT_PATH_DISJOINT_UNION(void)
{
    struct path_disjoint_union val;
    init_path_disjoint_union(&val);
    CU_ASSERT_EQUAL(val.path, NULL);
    CU_ASSERT_EQUAL(val.npaths, 0);
}

void test_INIT_PATH(void)
{
    struct path p;
    init_path(&p, 5);
    for(short i = 0; i < 5; i++)
    {
        p.metrics[i] = i;
    }
    CU_ASSERT_EQUAL(p.nvertices, 0);
    CU_ASSERT_EQUAL(p.vertice, NULL);
    CU_ASSERT_EQUAL(p.metrics[4], 4);
}

#endif // T_COMMON_H