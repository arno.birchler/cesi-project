export DEBUG=yes
export CC=gcc
#CC=x86_64-w64-mingw32-gcc for windows

ifeq ($(DEBUG),yes)
	export CFLAGS=-W -Wall -pedantic -g
	export LDFLAGS=
else
	export CFLAGS=-W -Wall -pedantic
	export LDFLAGS=
endif
SRC= $(wildcard *.c)
OBJ= $(SRC:.c=.o)

all: app

app:
	@(cd $@ && $(MAKE))

tests:
	@(cd $@ && $(MAKE))

.PHONY: app tests clean mrproper

clean:
	@rm -rf *.o
	@(cd app && $(MAKE) clean)
	@(cd tests && $(MAKE) clean)

mrproper: clean
	@rm -rf bin/*.bin